import React from "react";
import ReactDOM from "react-dom";
import Card from "./Card";

import { mount } from "enzyme";

const card = { number: "K", suit: { abbrev: "H" } };

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Card card={card}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("renders the appropriate class names", () => {
  const component = mount(<Card card={card}/>);
  expect(component.find(".card").length).toBe(1);
});


it("renders the correct unicode symbol for suits", () => {
  const component = mount(<Card card={card}/>);
  expect(component.find(".card-suit-red").length).toBe(1);
});
