import React from "react";
import PropTypes from "prop-types";
import Stack from "./Stack";

function TableGrid({ stacks, onDropCard }) {
  return (
    <div className="grid">
      <Stack cards={stacks.NW} position="NW" onDropCard={onDropCard}/>
      <Stack cards={stacks.N} position="N" onDropCard={onDropCard}/>
      <Stack cards={stacks.NE} position="NE" onDropCard={onDropCard}/>
      <Stack cards={stacks.W} position="W" onDropCard={onDropCard}/>
      <div className="cell">
        <div className="card faceDown"></div>
      </div>
      <Stack cards={stacks.E} position="E" onDropCard={onDropCard}/>
      <Stack cards={stacks.SW} position="SW" onDropCard={onDropCard}/>
      <Stack cards={stacks.S} position="S" onDropCard={onDropCard}/>
      <Stack cards={stacks.SE} position="SE" onDropCard={onDropCard}/>
    </div>
  );
}

TableGrid.propTypes = {
  stacks: PropTypes.shape({
    N: PropTypes.array.isRequired,
    E: PropTypes.array.isRequired,
    S: PropTypes.array.isRequired,
    W: PropTypes.array.isRequired,
    NE: PropTypes.array.isRequired,
    NW: PropTypes.array.isRequired,
    SE: PropTypes.array.isRequired,
    SW: PropTypes.array.isRequired
  }),
  onDropCard: PropTypes.func.isRequired
};

export default TableGrid;
