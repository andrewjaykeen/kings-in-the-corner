import React from "react";
import PropTypes from "prop-types";
import Card from "./Card";

function PlayerSpot({
  name,
  isPlayersTurn = false,
  cards = [],
  showCards = false,
  onEndTurnClick
}) {
  return (
    <div className={`player-spot ${isPlayersTurn ? "is-player-turn" : ""}`}>
      <span className="player-name">{name}</span>
      <span className="num-cards">{cards.length} cards</span>
      {showCards && (
        <div className="player-hand">
          {cards.map(card => {
            return (
              <Card
                key={`${card.number}${card.suit.abbrev}`}
                card={card}
              ></Card>
            );
          })}
        </div>
      )}
      {onEndTurnClick && <button onClick={onEndTurnClick} className="end-turn-button">End Turn</button>}
    </div>
  );
}

PlayerSpot.propTypes = {
  name: PropTypes.string.isRequired,
  isPlayersTurn: PropTypes.bool,
  cards: PropTypes.array.isRequired,
  showCards: PropTypes.bool,
  onEndTurnClick: PropTypes.func
};

export default PlayerSpot;
