import React from "react";
import PropTypes from "prop-types";

const SUITS = {
  C: <span className="card-suit-black">♣</span>,
  H: <span className="card-suit-red">♥</span>,
  S: <span className="card-suit-black">♠</span>,
  D: <span className="card-suit-red">♦</span>
};

const renderSuitSymbol = abbrev => {
  const jsx = SUITS[abbrev];
  return jsx ? jsx : <span>?</span>;
};

const onDrag = e => {
  const sourceCardNumber = e.target.getAttribute("data-card-number");
  const sourceCardSuit = e.target.getAttribute("data-card-suit");
  const sourceStackPosition = e.target.parentNode.getAttribute(
    "data-stack-position"
  );
  e.dataTransfer.setData("sourceCardNumber", sourceCardNumber);
  e.dataTransfer.setData("sourceCardSuit", sourceCardSuit);
  e.dataTransfer.setData("sourceStackPosition", sourceStackPosition);
};

function Card({ card }) {
  return (
    <div
      data-card-number={card.number}
      data-card-suit={card.suit.abbrev}
      className="card"
      draggable={true}
      onDragStart={onDrag}
    >
      {card.number}
      {renderSuitSymbol(card.suit.abbrev)}
    </div>
  );
}

Card.propTypes = {
  card: PropTypes.object.isRequired
};

export default Card;
