import React from "react";
import ReactDOM from "react-dom";
import PlayerSpot from "./PlayerSpot";
import { mount } from "enzyme";

const cards = [
  {
    number: "5",
    suit: { abbrev: "H" }
  },
  {
    number: "5",
    suit: { abbrev: "C" }
  }
];

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <PlayerSpot
      name="Test Player"
      isPlayersTurn={false}
      cards={cards}
      showCards={false}
    />,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});

it("renders the players name", () => {
  const component = mount(
    <PlayerSpot
      name="Test Player"
      isPlayersTurn={false}
      cards={cards}
      showCards={false}
    />
  );
  expect(component.find(".player-spot .player-name").length).toBe(1);
  expect(component.find(".player-spot .player-name").text()).toBe(
    "Test Player"
  );
});

it("renders the players card count", () => {
  const component = mount(
    <PlayerSpot
      name="Test Player"
      isPlayersTurn={false}
      cards={cards}
      showCards={false}
    />
  );
  expect(component.find(".player-spot .num-cards").length).toBe(1);
  expect(component.find(".player-spot .num-cards").text()).toBe("2 cards");
});

it("renders turn indicator", () => {
  const component = mount(
    <PlayerSpot
      name="Test Player"
      isPlayersTurn={true}
      cards={cards}
      showCards={false}
    />
  );
  expect(component.find(".player-spot.is-player-turn").length).toBe(1);
});
