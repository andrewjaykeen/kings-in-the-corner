import React from "react";
import PropTypes from "prop-types";

function Controls({ show = false, delay, onDelayChange, onStartSimClick, onStartSinglePlayerGame }) {
  if (show) {
    return (
      <div className="controls">
        <div className="delay-input">
          <label>Delay (ms): </label>
          <input type="number" value={delay} onChange={onDelayChange} />
        </div>
        <button onClick={onStartSimClick} className="start-sim-button">Start Sim</button>
        <button onClick={onStartSinglePlayerGame} className="start-single-player-game-button">Start Single Player Game</button>
      </div>
    );
  }
  return <div></div>;
}

Controls.propTypes = {
  show: PropTypes.bool.isRequired,
  onDelayChange: PropTypes.func.isRequired,
  onStartSimClick: PropTypes.func.isRequired,
  onStartSinglePlayerGame: PropTypes.func.isRequired,
};

export default Controls;
