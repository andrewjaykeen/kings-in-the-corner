import React from "react";
import ReactDOM from "react-dom";
import Controls from "./Controls";
import { spy } from "sinon";
import { mount } from "enzyme";

function noop() {
  return {};
}

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <Controls
      onStartSimClick={noop}
      onStartSinglePlayerGame={noop}
      onDelayChange={noop}
      show={true}
      delay={400}
    />,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});

it("invokes the onStartSimClick callback", () => {
  const onStartSimClickSpy = spy();
  const component = mount(
    <Controls
      onStartSimClick={onStartSimClickSpy}
      onStartSinglePlayerGame={noop}
      onDelayChange={noop}
      show={true}
      delay={400}
    />
  );
  component
    .find("button.start-sim-button")
    .first()
    .simulate("click");
  expect(onStartSimClickSpy.calledOnce).toBe(true);
});
