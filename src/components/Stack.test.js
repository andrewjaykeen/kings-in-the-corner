import React from "react";
import ReactDOM from "react-dom";
import Stack from "./Stack";

import { mount } from "enzyme";
function noop() {
  return {};
}

const cards = [
  { number: "K", suit: { abbrev: "H" } },
  { number: "Q", suit: { abbrev: "S" } }
];

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Stack cards={[]} position="N" onDropCard={noop} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("renders the appropriate class names", () => {
  const component = mount(
    <Stack cards={cards} position="SE" onDropCard={noop} />
  );
  expect(component.find(".cell .card-holder.card-holder-SE").length).toBe(1);
});

it("renders the right amount of cards", () => {
  const component = mount(
    <Stack cards={cards} position="SE" onDropCard={noop} />
  );
  expect(component.find(".cell .card-holder .card").length).toBe(cards.length);
});

it("renders the correct unicode symbol for suits", () => {
  const component = mount(
    <Stack cards={cards} position="N" onDropCard={noop} />
  );
  expect(component.find(".cell .card-holder .card-suit-red").length).toBe(1);
});
