import React from "react";
import ReactDOM from "react-dom";
import TableGrid from "./TableGrid";
import Stack from "./Stack";

import { mount } from "enzyme";
function noop() {
  return {};
}

const stacks = {
  N: [{ number: "10", suit: { name: "Hearts", abbrev: "H" } }],
  E: [{ number: "2", suit: { name: "Hearts", abbrev: "H" } }],
  S: [{ number: "Q", suit: { name: "Spades", abbrev: "S" } }],
  W: [{ number: "J", suit: { name: "Hearts", abbrev: "H" } }],
  NE: [],
  SE: [],
  NW: [],
  SW: []
};

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<TableGrid stacks={stacks} onDropCard={noop} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("renders the appropriate class names", () => {
  const component = mount(<TableGrid stacks={stacks} onDropCard={noop} />);
  expect(component.find(".grid").length).toBe(1);
});

it("renders 8 stacks", () => {
  const component = mount(<TableGrid stacks={stacks} onDropCard={noop} />);
  expect(component.find(Stack).length).toBe(8);
});
