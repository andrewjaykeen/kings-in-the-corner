import React, { useState } from "react";
import PropTypes from "prop-types";
import Card from "./Card";
import _ from "lodash";

const onDrop = ({ setHovering, onDropCard }, e) => {
  setHovering(false);

  const sourceCardNumber = e.dataTransfer.getData("sourceCardNumber");
  const sourceCardSuit = e.dataTransfer.getData("sourceCardSuit");
  const sourceStackPosition = e.dataTransfer.getData("sourceStackPosition");

  let targetStackPosition = e.target.getAttribute("data-stack-position");
  if (!targetStackPosition) {
    targetStackPosition = e.target.parentNode.getAttribute(
      "data-stack-position"
    );
  }
  if (!targetStackPosition) {
    targetStackPosition = e.target.parentNode.parentNode.getAttribute(
      "data-stack-position"
    );
  }

  // console.log(`----`);
  // console.log(
  //   `DEBUG: STACK onDrop: source: ${sourceCardNumber} ${sourceCardSuit} (stack = ${sourceStackPosition})`
  // );
  // console.log(`DEBUG: STACK onDrop: target: stack = ${targetStackPosition}`);

  if (onDropCard) {
    onDropCard({
      sourceCardNumber,
      sourceCardSuit,
      sourceStackPosition,
      targetStackPosition
    });
  }
};

const allowDrop = ({ hovering, setHovering }, e) => {
  // console.log('allow drop?');
  e.preventDefault();
  if (!hovering) {
    setHovering(true);
  }
};

const onDragLeave = ({ hovering, setHovering }) => {
  if (hovering) {
    setHovering(false);
  }
};

function Stack({ cards, position, onDropCard }) {
  const [hovering, setHovering] = useState(false);
  return (
    <div
      className={`cell ${hovering ? "drag-hovering" : ""}`}
      onDrop={_.partial(onDrop, { setHovering, onDropCard })}
      onDragOver={_.partial(allowDrop, { hovering, setHovering })}
      onDragLeave={_.partial(onDragLeave, { hovering, setHovering })}
      data-stack-position={position}
    >
      <div
        className={`card-holder card-holder-${position}`}
        data-stack-position={position}
      >
        {cards.map(card => {
          return <Card key={`${card.number}${card.suit.abbrev}`} card={card} />;
        })}
      </div>
    </div>
  );
}

Stack.propTypes = {
  cards: PropTypes.arrayOf(
    PropTypes.shape({
      number: PropTypes.string.isRequired,
      suit: PropTypes.object.isRequired
    })
  ).isRequired,
  position: PropTypes.string.isRequired,
  onDropCard: PropTypes.func.isRequired
};

export default Stack;
