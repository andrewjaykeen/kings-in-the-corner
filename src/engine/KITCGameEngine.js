const CARD_NUMBER_LIST = [
  "A",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "J",
  "Q",
  "K"
];
const GAME_STATES = {
  NEW: "New",
  PLAYING: "Playing",
  COMPLETED: "Completed"
};
const SUIT_ORDER = ["H", "C", "D", "S"]; // the order that the suits appear in the player's hand
const SUITS = [
  { name: "Hearts", abbrev: "H" },
  { name: "Diamonds", abbrev: "D" },
  { name: "Clubs", abbrev: "C" },
  { name: "Spades", abbrev: "S" }
];

class KITCGameEngine {
  constructor(config) {
    this.playerNames = config.playerNames || ["Player 1", "Player 2"];
    this.stateChangeCallback = config.stateChangeCallback || null;
    this.printDebugInfo = config.printDebugInfo || null;

    this.gameState = GAME_STATES.NEW;
    this.winner = -1; // possible values: 1 (player 1), 2 (player 2), 0 (a tie game / draw), or -1 (Not Yet finished)

    this.currentPlayersTurn = -1; // who's turn is it?  possible values: 1 (player 1), 2 (player 2) or -1 (Not Applicable)
    this.hasDrawnCard = false; // has the current user drawn a card yet

    this.deck = []; // array of cards (really only used for dealing)
    this.playerHands = []; // array of arrays
    this.stockPile = []; // array of card objects

    this.stacks = {
      N: [],
      E: [],
      S: [],
      W: [],
      NE: [],
      SE: [],
      NW: [],
      SW: []
    };

    this.init();
  }

  getGameStateString = () => {
    const theGameState = this.getGameState();
    return `
      =========== GAME STATE =========
      player names = ${theGameState.playerNames}
      game state = ${theGameState.state}
      winner = ${theGameState.winner}
      currentPlayersTurn = ${theGameState.currentPlayersTurn}
      stockPile = ${this.toStrArray(theGameState.stockPile)}
      player 1 hand = ${this.toStrArray(theGameState.playerHands[0])}
      player 2 hand = ${this.toStrArray(theGameState.playerHands[1])}
      stack N = ${this.toStrArray(theGameState.stacks.N)}
      stack E = ${this.toStrArray(theGameState.stacks.E)}
      stack S = ${this.toStrArray(theGameState.stacks.S)}
      stack W = ${this.toStrArray(theGameState.stacks.W)}
      stack NW = ${this.toStrArray(theGameState.stacks.NW)}
      stack SW = ${this.toStrArray(theGameState.stacks.SW)}
      stack NE = ${this.toStrArray(theGameState.stacks.NE)}
      stack SE = ${this.toStrArray(theGameState.stacks.SE)}
      =========== GAME STATE =========
    `;
  };

  toStrArray(arr) {
    return arr.map(card => {
      return card.suit ? `${card.number}${card.suit.abbrev}` : "ERROR";
    });
  }

  setState = newState => {
    const self = this;
    self.gameState = newState;
    if (self.stateChangeCallback) {
      self.stateChangeCallback();
    }
  };

  getGameState = () => {
    return {
      state: this.gameState,
      playerNames: this.playerNames,
      winner: this.winner,
      currentPlayersTurn: this.currentPlayersTurn,
      stockPile: this.stockPile,
      playerHands: this.playerHands,
      stacks: this.stacks
    };
  };

  // sort the player hands (self.playerHands) in order of cardNumber and suit
  sortPlayerHands = () => {
    const self = this;

    for (let i = 0; i < 2; ++i) {
      let playerHand = self.playerHands[i];

      playerHand.sort(function(a, b) {
        // first sort by suit
        if (
          SUIT_ORDER.indexOf(a.suit.abbrev) < SUIT_ORDER.indexOf(b.suit.abbrev)
        )
          return -1;
        if (
          SUIT_ORDER.indexOf(a.suit.abbrev) > SUIT_ORDER.indexOf(b.suit.abbrev)
        )
          return 1;

        // if the suits are even, then sort by card number
        if (
          CARD_NUMBER_LIST.indexOf(a.number) <
          CARD_NUMBER_LIST.indexOf(b.number)
        )
          return -1;
        if (
          CARD_NUMBER_LIST.indexOf(a.number) >
          CARD_NUMBER_LIST.indexOf(b.number)
        )
          return 1;

        // invalid case - no two people should ever have the same exact card in thier hand
        return 0;
      });
    }
  };

  dealCards = () => {
    const CARDS_PER_PLAYER = 7;
    this.playerHands[0] = this.deck.slice(0, CARDS_PER_PLAYER); // player 1
    this.playerHands[1] = this.deck.slice(
      CARDS_PER_PLAYER,
      CARDS_PER_PLAYER * 2
    ); // player 2
    this.stockPile = this.deck.slice(CARDS_PER_PLAYER * 2, 52);

    this.stacks.N.push(this.stockPile.splice(this.stockPile.length - 1, 1)[0]);
    this.stacks.S.push(this.stockPile.splice(this.stockPile.length - 1, 1)[0]);
    this.stacks.E.push(this.stockPile.splice(this.stockPile.length - 1, 1)[0]);
    this.stacks.W.push(this.stockPile.splice(this.stockPile.length - 1, 1)[0]);
  };

  shuffleDeck = () => {
    let currentIndex = this.deck.length,
      temporaryValue,
      randomIndex;

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = this.deck[currentIndex];
      this.deck[currentIndex] = this.deck[randomIndex];
      this.deck[randomIndex] = temporaryValue;
    }
  };

  createDeck = () => {
    for (let i = 0; i < SUITS.length; ++i) {
      let curSuit = SUITS[i];
      for (let j = 0; j < CARD_NUMBER_LIST.length; ++j) {
        let curCardNumber = CARD_NUMBER_LIST[j];
        let currentCard = { number: curCardNumber, suit: curSuit };
        this.deck.push(currentCard);
      }
    }
  };

  /*
   *  endTurn
   *
   * @param playerNumber: 1-based (1-2)
   * @return true on success, false for invalid state or params
   */
  endTurn = playerNumber => {
    const self = this;

    if (playerNumber !== self.currentPlayersTurn) {
      self.log(
        `ERROR: endTurn() invoked with a playerNumber that doesn't match self.currentPlayersTurn: ${playerNumber} and ${self.currentPlayersTurn}`
      );
      return false;
    }

    if (!self.hasDrawnCard) {
      self.log(
        `ERROR: endTurn() invoked, but the player hasn't drawn a card yet (hasDrawnCard=false): ${playerNumber} and ${self.currentPlayersTurn}`
      );
      return false;
    }

    // adjust/increment self.currentPlayersTurn
    if (playerNumber === 2) {
      self.currentPlayersTurn = 1;
    } else {
      self.currentPlayersTurn++;
    }

    self.hasDrawnCard = false; // reset

    return true;
  };

  /*
   *  drawCard
   *
   * @param playerNumber: 1-based (1-2)
   * @return true on success, false for invalid state or params
   */
  drawCard = playerNumber => {
    const self = this;

    if (playerNumber !== self.currentPlayersTurn) {
      self.log(
        `ERROR: drawCard() invoked with a playerNumber that doesn't match self.currentPlayersTurn: ${playerNumber} and ${self.currentPlayersTurn}`
      );
      return false;
    }

    if (self.hasDrawnCard) {
      self.log(
        `ERROR: drawCard() invoked, but the user has already drawn a card (hasDrawnCard=true): ${playerNumber} and ${self.currentPlayersTurn}`
      );
      return false;
    }

    if (this.stockPile.length < 1) {
      // TODO: handle this condition!  the stockpile is empty!
    }

    // pop one off the stockpile, and put it in the players hand
    const card = this.stockPile.pop();
    this.playerHands[playerNumber - 1].push(card);

    self.hasDrawnCard = true;

    return true;
  };

  /*
   *  getIndexOfCardInStack
   *
   * @param cardNumber: string: 2,3,4,K, etc
   * @param cardSuit: string: H, D, C, S
   * @param stackName: string: N,W,S,E,NW,SW,NE,SE
   * @return sourceIndex: int
   */
  getIndexOfCardInStack = (cardNumber, cardSuit, stackName) => {
    // TODO: just add a getIndexOfCardInStack(cardNumber, cardSuit, stackName) to the game engine.
    const stack = this.stacks[stackName];
    let foundIndex = -1;
    stack.forEach((card, index) => {
      if (card.number === cardNumber && card.suit.abbrev === cardSuit) {
        foundIndex = index;
      }
    });
    return foundIndex;
  };

  /*
   *  canMoveCards
   *
   * @param playerNumber: 1-based (1-4)
   * @param sourceStack: string: N,W,S,E,NW,SW,NE,SE
   * @param sourceIndex: int: 0-n (where n is the sourceStack.length-1)
   * @param targetStack: string: N,W,S,E,NW,SW,NE,SE
   * @return true on success, false for invalid state or params
   */
  canMoveCards = (playerNumber, sourceStack, sourceIndex, targetStack) => {
    const self = this;

    if (playerNumber !== self.currentPlayersTurn) {
      self.log(
        "ERROR: canMoveCards() invoked with a playerNumber that doesn't match self.currentPlayersTurn: " +
          playerNumber +
          " and: " +
          self.currentPlayersTurn
      );
      return false;
    }

    if (!self.hasDrawnCard) {
      self.log(
        `ERROR: canMoveCards() invoked but the player hasn't drawn a card yet. playerNumber = ${playerNumber}`
      );
      return false;
    }

    // first verify that the sourceStack has at least 1 card in it (sourceStack.length > 0)
    if (!this.stacks[sourceStack] || this.stacks[sourceStack].length < 1) {
      self.log(
        "ERROR: canMoveCards() invoked but the source stack length is less than one!"
      );
      return false;
    }
    // verify that the sourceIndex is valid (ie: it's less than sourceStack.length)
    if (!(sourceIndex >= 0 && sourceIndex < this.stacks[sourceStack].length)) {
      self.log(
        `ERROR: canMoveCards() invoked but the source index is invalid: sourceIndex = ${sourceIndex}, sourceStack.length = ${this.stacks[sourceStack].length}`
      );
      return false;
    }

    // inspect the card that's at sourceStack[sourceIndex] and compare it to the very last card of targetStack.
    if (this.stacks[targetStack].length > 0) {
      // if there is at least 1 card on the target stack, then...
      const sourceCard = this.stacks[sourceStack][sourceIndex];
      const targetCard = this.stacks[targetStack][
        this.stacks[targetStack].length - 1
      ];
      // check if the last card on the targetStack is 1 higher in rank and a different color.  if it's not, return false.
      if (!this.compareCards(sourceCard, targetCard)) {
        return false;
      }
    } else {
      // since the targetStack is empty, we need to check if its a "corner" and ensure they're not trying to move a non-King card there.
      const sourceCard = this.stacks[sourceStack][sourceIndex];
      if (
        ["NW", "SW", "SE", "NE"].includes(targetStack) &&
        sourceCard.number !== "K"
      ) {
        self.log(
          `ERROR: canMoveCards() invoked but they're trying to move a card that is not a king to an empty corner!`
        );
        return false;
      }
    }

    return true;
  };

  /*
   *  moveCards
   *
   * @param playerNumber: 1-based (1-4)
   * @param sourceStack: string: N,W,S,E,NW,SW,NE,SE
   * @param sourceIndex: int: 0-n (where n is the sourceStack.length-1)
   * @param targetStack: string: N,W,S,E,NW,SW,NE,SE
   * @return true on success, false for invalid state or params
   */
  moveCards = (playerNumber, sourceStack, sourceIndex, targetStack) => {
    const self = this;
    if (!self.canMoveCards(playerNumber, sourceStack, sourceIndex, targetStack))
      return false;
    // validation above was successful, so now actually move the cards.
    const ss = this.stacks[sourceStack];
    this.stacks[targetStack] = this.stacks[targetStack].concat(
      ss.splice(sourceIndex, ss.length)
    );
    return true; // success!
  };

  /*
   *  canPlayCard
   *
   * @param playerNumber: 1-based (1-4)
   * @param card: object:  { number: "3", { suit: { abbrev: "S" } } }
   * @param position: string: N,W,S,E,NW,SW,NE,SE
   * @return true on success, false for invalid state or params
   */
  canPlayCard = (playerNumber, card, position) => {
    const self = this;

    if (playerNumber !== self.currentPlayersTurn) {
      self.log(
        "ERROR: canPlayCard() invoked with a playerNumber that doesn't match self.currentPlayersTurn: " +
          playerNumber +
          " and: " +
          self.currentPlayersTurn
      );
      return false;
    }

    if (!self.hasDrawnCard) {
      self.log(
        `ERROR: canPlayCard() invoked but the player hasn't drawn a card yet: ${playerNumber}`
      );
      return false;
    }

    // ensure the user has the card they're attempting to play
    let playerHand = self.playerHands[playerNumber - 1],
      foundCardInPlayersHand = false;

    for (let i = 0; i < playerHand.length; ++i) {
      let curCard = playerHand[i];
      if (
        curCard.number === card.number &&
        curCard.suit.abbrev === card.suit.abbrev
      )
        foundCardInPlayersHand = true;
    }

    if (!foundCardInPlayersHand) {
      self.log(
        `ERROR: canPlayCard() invoked with a card that the player does not have in their hand: ${card.number} ${card.suit}`
      );
      return false;
    }

    // check if the stack is empty
    if (!self.stacks[position]) {
      self.log("ERROR: canPlayCard() invoked with a bad position: " + position);
      return false;
    }

    const stack = self.stacks[position];

    if (stack.length < 1) {
      // stack is empty, can only play kings in the corners (that's the name of the game!)
      if (
        ["NE", "SE", "NW", "SW"].indexOf(position) >= 0 &&
        card.number !== "K"
      ) {
        self.log(
          "ERROR: canPlayCard() attempting to play a card that is not a king, in a corner position: " +
            position
        );
        return false;
      }
    } else {
      // make sure that the ranking of the new card is 1 lower in number, and an opposite color (Hearts/Diamonds vs. Clubs/Spades).
      const lastCardInStack = stack[stack.length - 1];

      if (!this.compareCards(card, lastCardInStack)) {
        return false;
      }
    }

    return true;
  };

  /*
   *  playCard
   *
   * @param playerNumber: 1-based (1-4)
   * @param card: object:  { number: "3", suit: "S" }
   * @param position: string: N,W,S,E,NW,SW,NE,SE
   * @return true on success, false for invalid state or params
   */
  playCard = (playerNumber, card, position) => {
    const self = this;

    if (!self.canPlayCard(playerNumber, card, position)) return false;

    // validation above was successful.
    // now remove the card from the players hand and put it on the stack for the given position.

    // note: we must loop backwards since we're removing from the array as we iterate.
    let playerHand = self.playerHands[playerNumber - 1];
    let len = playerHand.length;
    while (len--) {
      let curCard = playerHand[len];
      if (
        curCard.number === card.number &&
        curCard.suit.abbrev === card.suit.abbrev
      ) {
        playerHand.splice(len, 1); // remove
        self.stacks[position].push(curCard); // push it on the stack
      }
    }

    // if the player is out of cards, they win! game over.
    if (playerHand.length === 0) {
      this.winner = playerNumber;
      this.currentPlayersTurn = -1;
      this.setState(GAME_STATES.COMPLETED);
    }

    if (this.stockPile.length === 0) {
      this.winner = 0; // indicative of a tie game (aka a "draw")
      this.currentPlayersTurn = -1;
      this.setState(GAME_STATES.COMPLETED);
    }

    return true; // success! card was played!
  };

  /*
   *  compareCards
   *
   * @param cardOne: object:  { number: "3", suit: "S" }
   * @param cardTwo: object:  { number: "4", suit: "H" }
   * @return true if you can play cardOne on cardTwo, meaning cardOne is 1 lower number rank and the opposite color than card two.
   */
  compareCards = (cardOne, cardTwo) => {
    // rank order check
    const indexOfCardOne = CARD_NUMBER_LIST.indexOf(cardOne.number);
    const indexOfCardTwo = CARD_NUMBER_LIST.indexOf(cardTwo.number);

    if (indexOfCardTwo - indexOfCardOne !== 1) {
      this.log(
        `ERROR: compareCards() attempting to play a card that is not in rank order: ${cardOne.number} of ${cardOne.suit.abbrev}`
      );
      return false;
    }

    // opposite color check
    if (
      (cardOne.suit.abbrev === "C" || cardOne.suit.abbrev === "S") &&
      (cardTwo.suit.abbrev === "C" || cardTwo.suit.abbrev === "S")
    ) {
      this.log(
        "ERROR: compareCards() attempting to play a card that is not the opposite color: " +
          cardOne
      );
      return false;
    }
    if (
      (cardOne.suit.abbrev === "H" || cardOne.suit.abbrev === "D") &&
      (cardTwo.suit.abbrev === "H" || cardTwo.suit.abbrev === "D")
    ) {
      this.log(
        "ERROR: compareCards() attempting to play a card that is not the opposite color: " +
          cardOne
      );
      return false;
    }
    return true;
  };

  init = () => {
    this.log("initializing game...");

    this.createDeck();
    this.shuffleDeck();
    this.dealCards();
    this.sortPlayerHands();
    this.currentPlayersTurn = 1;
    this.hasDrawnCard = false;

    this.setState(GAME_STATES.PLAYING);
  };

  log = msg => {
    if (this.printDebugInfo) console.log("Engine: " + msg);
  };
}

export default KITCGameEngine;
export { SUIT_ORDER, CARD_NUMBER_LIST };
