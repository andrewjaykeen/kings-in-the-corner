import { CARD_NUMBER_LIST } from "./KITCGameEngine";

const sortPlayerHandHighToLow = playerHand => {
  // sort the hand high to low
  playerHand.sort(function(a, b) {
    // sort by card number
    if (CARD_NUMBER_LIST.indexOf(a.number) < CARD_NUMBER_LIST.indexOf(b.number))
      return 1;
    if (CARD_NUMBER_LIST.indexOf(a.number) > CARD_NUMBER_LIST.indexOf(b.number))
      return -1;

    return 0;
  });
  return playerHand;
};

const moveCards = ({ game, outterStack, innerStack, delay }) => {
  return new Promise(resolve => {
    // try and move
    const success = game.moveCards(
      game.currentPlayersTurn,
      outterStack,
      0,
      innerStack
    );
    if (success) {
      setTimeout(() => {
        resolve();
      }, delay);
    } else {
      resolve();
    }
  });
};

const playCards = ({ game, curCard, curPos, delay }) => {
  return new Promise(resolve => {
    const success = game.playCard(game.currentPlayersTurn, curCard, curPos);
    if (success) {
      setTimeout(() => {
        resolve();
      }, delay);
    } else {
      resolve();
    }
  });
};



/**
 * performTurn
 * NOTE: this mutates the game state as it executes, potentially performing many operations per turn.
 *
 * @param game - KITCGameEngine instance
 * @param printDebugInfo - boolean
 * @param delay - milliseconds to wait
 */
const performTurn = ({ game, printDebugInfo, delay }) => {
  return new Promise(async resolve => {
    // before we go about trying to play cards from the players hand, see if we can moveCards() on the stacks
    // first.  for each stack, see if the FIRST CARD on that stack can be moved to any of the other non-empty stacks
    // (including corners). we can easily just brute-force this by attempting all combinations. if it fails, oh well.
    // this should free up space on the stacks before we start playing random cards from the players hand.

    const outterStacks = Object.keys(game.stacks);

    for (let i = 0; i < outterStacks.length; ++i) {
      const outterStack = outterStacks[i];
      const innerStacks = Object.keys(game.stacks);
      for (let j = 0; j < innerStacks.length; ++j) {
        const innerStack = innerStacks[j];
        // we can still try and move to an empty stack, but only if its a corner (could be a king!)
        if (
          game.stacks[innerStack].length > 0 ||
          ["NW", "SW", "SE", "NE"].includes(innerStack)
        ) {
          // avoid moving kings from one corner to another, that's useless
          if(!["NW", "SW", "SE", "NE"].includes(outterStack)) {
            await moveCards({
              game,
              outterStack,
              innerStack,
              delay
            });
          }
        }
      }
    }

    // we must copy the players hand, as the hand will be mutated by the game engine as cards are played
    let curPlayersHand = game.playerHands[game.currentPlayersTurn - 1].slice(0);

    curPlayersHand = sortPlayerHandHighToLow(curPlayersHand);
    const positions = Object.keys(game.stacks);

    for (var i = 0; i < curPlayersHand.length; ++i) {
      const curCard = curPlayersHand[i];
      // console.log(`${i} : curCard = ${curCard.number} of ${curCard.suit.abbrev}`);
      for (var j = 0; j < positions.length; ++j) {
        let gameIsOver = game.gameState === "Completed";
        const curPos = positions[j];

        if (!gameIsOver) {
          const success = await playCards({
            game,
            curCard,
            curPos,
            delay
          });

          if (success) {
            if (printDebugInfo) {
              console.log(
                `Player ${game.currentPlayersTurn} just played ${curCard.number} of ${curCard.suit.abbrev} at position ${curPos}`
              );
            }

            gameIsOver = game.gameState === "Completed";
          }
        }
      }
    }
    resolve();
  });
};

export default { performTurn, sortPlayerHandHighToLow };
