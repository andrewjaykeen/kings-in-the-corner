
import KITCRobot from "./KITCRobot";

const runGameSimulation = async (game, printDebugInfo, delay = 0) => {
  return new Promise( async resolve => {
    let gameState = game.getGameState(),
      loopCount = 0;
    while (gameState.state !== "Completed") {
      if (loopCount > 100) {
        console.log("running too long");
        break;
      }
      gameState = game.getGameState();
      if (printDebugInfo) {
        console.log(game.getGameStateString());
        console.log(`loop count = ${loopCount}`);
      }
      ++loopCount;

      let gameIsOver = game.gameState === "Completed";

      // try and play all their cards in all positions
      if (!gameIsOver) {
        if (!game.hasDrawnCard) {
          game.drawCard(game.currentPlayersTurn);
        }

        await KITCRobot.performTurn({ game, printDebugInfo, delay });

        if (game.gameState !== "Completed") {
          game.endTurn(game.currentPlayersTurn);
        }
      }
    }
    if (printDebugInfo) {
      console.log(game.getGameStateString());
    }
    resolve(gameState);
  });
};

const TEST_PLAYER_HANDS = [
  [
    { number: "A", suit: { name: "Hearts", abbrev: "H" } },
    { number: "A", suit: { name: "Spades", abbrev: "S" } },
    { number: "5", suit: { name: "Diamonds", abbrev: "D" } },
    { number: "7", suit: { name: "Diamonds", abbrev: "D" } },
    { number: "Q", suit: { name: "Diamonds", abbrev: "D" } },
    { number: "3", suit: { name: "Spades", abbrev: "S" } },
    { number: "K", suit: { name: "Spades", abbrev: "S" } }
  ],
  [
    { number: "5", suit: { name: "Hearts", abbrev: "H" } },
    { number: "6", suit: { name: "Clubs", abbrev: "C" } },
    { number: "4", suit: { name: "Diamonds", abbrev: "D" } },
    { number: "J", suit: { name: "Diamonds", abbrev: "D" } },
    { number: "4", suit: { name: "Spades", abbrev: "S" } },
    { number: "5", suit: { name: "Spades", abbrev: "S" } },
    { number: "9", suit: { name: "Spades", abbrev: "S" } }
  ]
];
const TEST_STOCK_PILE = [
  { number: "7", suit: { name: "Clubs", abbrev: "C" } },
  { number: "8", suit: { name: "Diamonds", abbrev: "D" } },
  { number: "A", suit: { name: "Diamonds", abbrev: "D" } },
  { number: "J", suit: { name: "Spades", abbrev: "S" } },
  { number: "5", suit: { name: "Clubs", abbrev: "C" } },
  { number: "2", suit: { name: "Clubs", abbrev: "C" } },
  { number: "Q", suit: { name: "Clubs", abbrev: "C" } },
  { number: "K", suit: { name: "Hearts", abbrev: "H" } },
  { number: "2", suit: { name: "Diamonds", abbrev: "D" } },
  { number: "K", suit: { name: "Diamonds", abbrev: "D" } },
  { number: "K", suit: { name: "Clubs", abbrev: "C" } },
  { number: "A", suit: { name: "Clubs", abbrev: "C" } },
  { number: "8", suit: { name: "Clubs", abbrev: "C" } },
  { number: "10", suit: { name: "Clubs", abbrev: "C" } },
  { number: "3", suit: { name: "Hearts", abbrev: "H" } },
  { number: "7", suit: { name: "Hearts", abbrev: "H" } },
  { number: "8", suit: { name: "Spades", abbrev: "S" } },
  { number: "4", suit: { name: "Hearts", abbrev: "H" } },
  { number: "10", suit: { name: "Spades", abbrev: "S" } },
  { number: "J", suit: { name: "Clubs", abbrev: "C" } },
  { number: "3", suit: { name: "Clubs", abbrev: "C" } },
  { number: "9", suit: { name: "Hearts", abbrev: "H" } },
  { number: "4", suit: { name: "Clubs", abbrev: "C" } },
  { number: "6", suit: { name: "Hearts", abbrev: "H" } },
  { number: "Q", suit: { name: "Hearts", abbrev: "H" } },
  { number: "3", suit: { name: "Diamonds", abbrev: "D" } },
  { number: "7", suit: { name: "Spades", abbrev: "S" } },
  { number: "2", suit: { name: "Spades", abbrev: "S" } },
  { number: "6", suit: { name: "Diamonds", abbrev: "D" } },
  { number: "6", suit: { name: "Spades", abbrev: "S" } },
  { number: "9", suit: { name: "Clubs", abbrev: "C" } },
  { number: "10", suit: { name: "Diamonds", abbrev: "D" } },
  { number: "9", suit: { name: "Diamonds", abbrev: "D" } },
  { number: "8", suit: { name: "Hearts", abbrev: "H" } }
];

const TEST_STACKS = {
  N: [{ number: "10", suit: { name: "Hearts", abbrev: "H" } }],
  E: [{ number: "2", suit: { name: "Hearts", abbrev: "H" } }],
  S: [{ number: "Q", suit: { name: "Spades", abbrev: "S" } }],
  W: [{ number: "J", suit: { name: "Hearts", abbrev: "H" } }],
  NE: [],
  SE: [],
  NW: [],
  SW: []
};

const setupTestData = game => {
  game.stacks = Object.assign({}, TEST_STACKS);
  game.stockPile = JSON.parse(JSON.stringify(TEST_STOCK_PILE));
  game.playerHands = JSON.parse(JSON.stringify(TEST_PLAYER_HANDS));
};

export { runGameSimulation, setupTestData };
