import { runGameSimulation, setupTestData } from "./TestUtils";
import KITCGameEngine from "./KITCGameEngine";

function noop() {
  return {};
}

const PLAYER_NAMES = ["You", "The Computer"];
const gameOptions = {
  playerNames: PLAYER_NAMES,
  printDebugInfo: false,
  stateChangeCallback: noop
};

describe("constructor", () => {
  it("new game honors options", () => {
    const game = new KITCGameEngine(gameOptions);
    const gameState = game.getGameState();

    expect(gameState.state).toEqual("Playing");
    expect(gameState.playerNames).toEqual(PLAYER_NAMES);
    expect(gameState.winner).toEqual(-1);
  });
});

describe("drawCard", () => {
  it("don't allow drawing a card if it's not their turn", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });

    //console.log(game.getGameStateString());
    expect(game.getGameState().currentPlayersTurn).toBe(1);
    const result = game.drawCard(2);
    expect(result).toBe(false);
  });
  it("user can only draw one card per turn", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });

    //console.log(game.getGameStateString());
    expect(game.getGameState().currentPlayersTurn).toBe(1);
    let result = game.drawCard(1);
    expect(result).toBe(true);
    result = game.drawCard(1);
    expect(result).toBe(false);
  });
});

describe("endTurn", () => {
  it("don't allow ending turn if it's not their turn", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });

    expect(game.getGameState().currentPlayersTurn).toBe(1);
    game.drawCard(1);
    const result = game.endTurn(2);
    expect(result).toBe(false);
  });
  it("don't allow ending turn if they haven't drawn a card yet", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });

    expect(game.getGameState().currentPlayersTurn).toBe(1);
    const result = game.endTurn(1);
    expect(result).toBe(false);
  });
  it("ensure the currentPlayersTurn changes", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });

    expect(game.getGameState().currentPlayersTurn).toBe(1);
    game.drawCard(1);
    const result = game.endTurn(1);
    expect(result).toBe(true);
    expect(game.getGameState().currentPlayersTurn).toBe(2);
  });
});

describe("playCard", () => {
  it(`don't allow playing a card if the card isn't the opposite suit`, () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    let result;
    setupTestData(game);
    game.drawCard(1);
    result = game.playCard(1, game.playerHands[0][0], "E"); // try to play AH on a 2H (different color)
    expect(result).toBe(false);
  });

  it(`don't allow playing a card if the card isn't a lower rank`, () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    let result;
    setupTestData(game);
    game.drawCard(1);
    result = game.playCard(1, game.playerHands[0][5], "N"); // try to play 3S on a 10H (same color, wrong rank)
    expect(result).toBe(false);
  });

  it(`don't allow playing a card the player doesn't have`, () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    expect(game.getGameState().currentPlayersTurn).toBe(1);
    game.drawCard(1);
    let result = game.playCard(1, game.playerHands[1][0], "N"); // the second player's first card in their hand
    expect(result).toBe(false);
  });

  it(`don't allow player to play card if it's not their turn `, () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    expect(game.getGameState().currentPlayersTurn).toBe(1);
    game.drawCard(1);
    let result = game.playCard(2, game.playerHands[1][0], "N"); // the first player's first card in their hand
    expect(result).toBe(false);
  });

  it(`can successfully play a card`, () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    let result;

    setupTestData(game);

    game.drawCard(1);
    result = game.playCard(1, game.playerHands[0][1], "E"); // try to play AS on a 2H (same color and rank, should work!)
    expect(result).toBe(true);
  });
});

describe("canMoveCards", () => {
  it(`don't allow player to move cards if it's not their turn `, () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    setupTestData(game);
    game.drawCard(1);
    let result = game.canMoveCards(2, "N", 0, "S");
    expect(result).toBe(false);
  });

  it("don't allow moving cards if they haven't drawn a card yet", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    setupTestData(game);
    expect(game.getGameState().currentPlayersTurn).toBe(1);
    let result = game.canMoveCards(2, "N", 0, "S");
    expect(result).toBe(false);
  });

  it("don't allow moving cards if the source card can't be played on the target card", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    setupTestData(game);
    game.drawCard(1);
    expect(game.getGameState().currentPlayersTurn).toBe(1);
    let result = game.canMoveCards(1, "N", 0, "S");
    expect(result).toBe(false);
  });

  it("will allow moving cards if the source card can be played on the target card", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    setupTestData(game);
    game.drawCard(1);
    let result = game.canMoveCards(1, "W", 0, "S"); // moving the JH on QS, a valid move..
    expect(result).toBe(true);
  });

  it("don't allow them to move a non-king card into the corner, if that corner is empty", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    setupTestData(game);
    game.drawCard(1);
    let result = game.canMoveCards(1, "W", 0, "SE"); // moving the JH into an empty corner, an invalid move
    expect(result).toBe(false);
  });

  // TODO: additional tests for moving multiple cards.
});

describe("canMoveCards", () => {
  it("will moving cards if the source card can be played on the target card", () => {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    setupTestData(game);
    game.drawCard(1);
    expect(game.stacks.S.length).toBe(1);
    expect(game.stacks.W.length).toBe(1);
    let result = game.moveCards(1, "W", 0, "S"); // moving the JH on QS, a valid move..
    expect(result).toBe(true);
    // console.log(game.getGameStateString());
    expect(game.stacks.S.length).toBe(2);
    expect(game.stacks.W.length).toBe(0);
  });
});

it(`can complete multiple game simulations, resulting in at least 50% wins`, async () => {
  let numWins = 0;
  let numTies = 0;
  const TOTAL_GAMES = 10;
  for (var i = 0; i < TOTAL_GAMES; ++i) {
    let game = new KITCGameEngine({
      printDebugInfo: false
    });
    let gameState = await runGameSimulation(game, false, 0);
    if (gameState.winner === 0) numTies++;
    if (gameState.winner > 0) numWins++;
    expect(gameState.winner).toBeGreaterThan(-1);
    expect(gameState.state).toBe("Completed");
  }
  console.log(`won ${numWins} out of ${TOTAL_GAMES}`)
  expect(numWins).toBeGreaterThan(5); // at least 5 win out of 10 games
});

it(`simulate a single game with a delay`, async () => {
  const start = new Date().getTime();
  let game = new KITCGameEngine({
    printDebugInfo: false
  });
  let gameState = await runGameSimulation(game, false, 2);  // only 2ms delay, we just need something non-zero
  const end = new Date().getTime();
  const delta = end-start;
  expect(delta).toBeGreaterThan(50);  // no game will complete is such a short time
  expect(gameState.winner).toBeGreaterThan(-1);
  expect(gameState.state).toBe("Completed");
});
