import KITCRobot from "./KITCRobot";

function noop() {
  return {};
}

describe("sortPlayerHandHighToLow", () => {
  it("sorts appropriately", () => {
    let playerHand = [
      { number: "J", suit: { abbrev: "C" } },
      { number: "K", suit: { abbrev: "H" } },
      { number: "2", suit: { abbrev: "S" } },
      { number: "5", suit: { abbrev: "H" } }
    ];
    KITCRobot.sortPlayerHandHighToLow(playerHand);
    expect(playerHand).toEqual([
      { number: "K", suit: { abbrev: "H" } },
      { number: "J", suit: { abbrev: "C" } },
      { number: "5", suit: { abbrev: "H" } },
      { number: "2", suit: { abbrev: "S" } }
    ]);
  });
});

// TODO: performTurn tests..
