import "./scss/App.scss";
import React, { Component } from "react";
import KITCGameEngine from "./engine/KITCGameEngine";
import { runGameSimulation } from "./engine/TestUtils";
import TableGrid from "./components/TableGrid";
import PlayerSpot from "./components/PlayerSpot";
import Controls from "./components/Controls";
import KITCRobot from "./engine/KITCRobot";

class App extends Component {
  state = {
    game: null,
    gameState: null,
    started: false,
    simulating: false,
    delay: 500
  };

  componentDidMount() {
    const game = new KITCGameEngine({ printDebugInfo: true });
    const self = this;
    this.setState({ game }, () => {
      // polling for state from the game for now.  this could just as easily be game event callbacks.
      // TODO: don't poll, just subscribe to callback events...
      const interval = setInterval(() => {
        const gameState = game.getGameState();
        if (gameState.state === "Completed") {
          clearInterval(interval);
        }
        self.setState({
          gameState
        });
      }, 250); // !! polling interval
      // this.startSinglePlayerGame(); // auto-start.  could be startGameSimulation() as well..
    });
  }

  handleDelayChange = e => {
    this.setState({ delay: e.target.value });
  };

  endTurn = e => {
    this.state.game.endTurn(1);
  };

  cardDropped = ({
    sourceCardNumber,
    sourceCardSuit,
    sourceStackPosition,
    targetStackPosition
  }) => {
    if (this.state.started && !this.state.simulating) {
      if (sourceStackPosition === "null") {
        // dragging from the players hand (so try and play the card at that position)
        this.state.game.playCard(
          1,
          { number: sourceCardNumber, suit: { abbrev: sourceCardSuit } },
          targetStackPosition
        );
      } else {
        // dragging from the board, to one of the stacks.
        // In order to try to move the cards, we need to now get the source INDEX.
        const sourceIndex = this.state.game.getIndexOfCardInStack(
          sourceCardNumber,
          sourceCardSuit,
          sourceStackPosition
        );
        this.state.game.moveCards(
          1,
          sourceStackPosition,
          sourceIndex,
          targetStackPosition
        );
      }
    }
  };

  singlePlayerGameLoop = () => {
    setTimeout(async () => {
      const game = this.state.game;
      if (game.state !== "Completed") {
        // make a move for player #2 (the robot opponent)
        let gameIsOver = game.state === "Completed";
        if (!gameIsOver) {
          if (!game.hasDrawnCard) {
            game.drawCard(game.currentPlayersTurn);
          }

          if (game.currentPlayersTurn === 2) {
            await KITCRobot.performTurn({
              game,
              printDebugInfo: true,
              delay: this.state.delay
            });
            game.endTurn(2);
          }
        }
        this.singlePlayerGameLoop();
      }
    }, 250);
  };

  startSinglePlayerGame = () => {
    this.setState({ started: true, simulating: false }, () => {
      this.singlePlayerGameLoop();
    });
  };

  startGameSimulation = () => {
    runGameSimulation(this.state.game, false, this.state.delay); // !! robot delay
    this.setState({ started: true, simulating: true });
  };

  renderWinner() {
    if (this.state.gameState.state !== "Completed") {
      return;
    } else if (this.state.gameState.winner === 0) {
      return <div style={{ marginTop: "50px" }}>It was a draw.</div>;
    }
    return (
      <div style={{ marginTop: "50px" }}>
        Winner:{" 🏆 "}
        {this.state.gameState.winner === 1 ? "Player One" : "Player Two"}
      </div>
    );
  }

  render() {
    if (!this.state.gameState) return <div>Loading...</div>;

    let onEndTurnClick;
    if (
      this.state.started &&
      !this.state.simulating &&
      this.state.gameState.currentPlayersTurn === 1
    ) {
      onEndTurnClick = this.endTurn;
    }

    return (
      <div className="App">
        <div
          style={{
            display: "flex",
            justifyContent: "space-around",
            minHeight: "800px",
            width: "1000px",
            margin: "auto"
          }}
        >
          <div className="left-side">
            <PlayerSpot
              name="Player 1"
              isPlayersTurn={this.state.gameState.currentPlayersTurn === 1}
              cards={this.state.gameState.playerHands[0]}
              showCards={true}
              onEndTurnClick={onEndTurnClick}
            />
            <PlayerSpot
              name="Player 2"
              isPlayersTurn={this.state.gameState.currentPlayersTurn === 2}
              cards={this.state.gameState.playerHands[1]}
              showCards={this.state.simulating}
            />
            {this.renderWinner()}
            <Controls
              show={!this.state.started}
              delay={this.state.delay}
              onDelayChange={this.handleDelayChange}
              onStartSimClick={this.startGameSimulation}
              onStartSinglePlayerGame={this.startSinglePlayerGame}
            />
          </div>
          <div className="right-side">
            <TableGrid
              stacks={this.state.gameState.stacks}
              onDropCard={this.cardDropped}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
